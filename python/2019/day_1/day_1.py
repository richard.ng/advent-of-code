import math

with open("input.txt") as file:
  puzzle_in = file.readlines()

def get_fuel(number):
  return math.floor(int(number) / 3) - 2

total = 0

for c, v in enumerate(puzzle_in):
  module = get_fuel(v)
  total += module

print(f"Total amount of fuel for all modules is {total}")

total = 0

for c, v in enumerate(puzzle_in):
  module = get_fuel(v)
  fuel = get_fuel(module)
  total += module + fuel
  while fuel > 0:
    fuel = get_fuel(fuel)
    if fuel < 0:
      break
    total += fuel

print(f"Total amount of fuel for all modules + fuel is {total}")
