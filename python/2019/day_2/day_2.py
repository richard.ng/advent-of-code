with open("input.txt") as file:
  puzzle_in = file.read()

split = puzzle_in.split(",")
for c, v in enumerate(split):
  split[c] = int(v)
original = split.copy()

split[1] = 12
split[2] = 2

for i in range(0,len(split),4):
  code = split[i:i+4]
  if code[0] == 1:
    split[code[3]] = split[code[1]] + split[code[2]]
  if code[0] == 2:
    split[code[3]] = split[code[1]] * split[code[2]]
  if code[0] == 99:
    break

print(f"Position 0 contains a value of {split[0]}")

for j in range(0,99):
  for k in range(0,99):
    split = original.copy()
    split[1] = j
    split[2] = k
    for i in range(0,len(split),4):
      code = split[i:i+4]
      if code[0] == 1:
        split[code[3]] = split[code[1]] + split[code[2]]
      if code[0] == 2:
        split[code[3]] = split[code[1]] * split[code[2]]
      if code[0] == 99:
        break
      if split[0] == 19690720:
        print(f"The value of 100 * noun + verb is {(100*j)+k}")
