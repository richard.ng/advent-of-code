with open("input.txt") as file:
  puzzle_in = file.read()

x = 0
y = 0
coords = []

for c, v in enumerate(puzzle_in):
  if "<" in v:
    x -= 1
  if "^" in v:
    y += 1
  if ">" in v:
    x += 1
  if "v" in v:
    y -= 1
  pos = x, y
  coords.append(pos)

print(f"The number of unique houses is {len(set(coords))}")

santa = puzzle_in[::2]
robo = puzzle_in[1::2]

x = 0
y = 0
coords = []

for c, v in enumerate(santa):
  if "<" in v:
    x -= 1
  if "^" in v:
    y += 1
  if ">" in v:
    x += 1
  if "v" in v:
    y -= 1
  pos = x, y
  coords.append(pos)

x = 0
y = 0
for c, v in enumerate(robo):
  if "<" in v:
    x -= 1
  if "^" in v:
    y += 1
  if ">" in v:
    x += 1
  if "v" in v:
    y -= 1
  pos = x, y
  coords.append(pos)

print(f"The number of unique houses is {len(set(coords))}")

