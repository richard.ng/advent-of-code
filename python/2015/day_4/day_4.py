import hashlib

puzzle_in = "bgvyzdsv"

for i in range(0,9999999):
  test = puzzle_in + str(i)
  test = test.encode()
  m = hashlib.md5(test)

  if m.hexdigest()[:5] == "00000":
    print(f"The secret key is {i}")
    break

for i in range(0,9999999):
  test = puzzle_in + str(i)
  test = test.encode()
  m = hashlib.md5(test)

  if m.hexdigest()[:6] == "000000":
    print(f"The secret key is {i}")
    break
