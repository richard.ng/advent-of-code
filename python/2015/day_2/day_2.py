with open("input.txt") as file:
  puzzle_in = file.readlines()

dimensions = []

for line in puzzle_in:
  dimensions.append(line.strip().split("x"))

for c, v in enumerate(dimensions):
  for j, k in enumerate(v):
    v[j] = int(k)

def get_sa(length, width, height):
  side1 = 2 * length * width
  side2 = 2 * length * height
  side3 = 2 * width * height
  slack = min(side1/2, side2/2, side3/2)
  return int(side1 + side2 + side3 + slack)

wrapping = 0
for c, v in enumerate(dimensions):
  wrapping += get_sa(v[0],v[1],v[2])

print(f"Total amount of wrapping paper needed is {wrapping}")

def get_perimeter(length, width, height):
  side1 = (2 * length) + (2 * width)
  side2 = (2 * length) + (2 * height)
  side3 = (2 * width) + (2 * height)
  bow = length * width * height
  return min(side1, side2, side3) + bow

ribbon = 0
for c, v in enumerate(dimensions):
  ribbon += get_perimeter(v[0],v[1],v[2])

print(f"Total amount of ribbon needed is {ribbon}")
