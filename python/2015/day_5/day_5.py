with open("input.txt") as file:
  puzzle_in = file.readlines()

vowels = ["a", "e", "i", "o", "u"]
forbidden = ["ab", "cd", "pq", "xy"]
naughty = 0

def vowel(line):
  count = 0
  for each in line:
    for letter in vowels:
      if letter == each:
        count += 1
  if count < 3:
    return False
  else:
    return True

def double(line):
  for c, v in enumerate(line):
    if c == 0 or c == len(line) - 1:
      pass
    elif v == line[c+1] or v == line[c-1]:
      return True
  return False

def disallowed(line):
  for bad in forbidden:
    if bad in line:
      return False
  return True

for line in puzzle_in:
  line = line.split()[0]

  if vowel(line) == False or double(line) == False or disallowed(line) == False:
    naughty += 1

print(f"Total number of nice strings is {len(puzzle_in) - naughty}")

naughty = 0

def pair(line):
  count = 0
  for c, v in enumerate(line):
    if c == len(line) - 1:
      pass
    else:
      p = line[c] + line[c+1]
      if line.count(p) >= 2:
        count += 1

  if count < 2:
    return False
  else:
    return True

def between(line):
  for c, v in enumerate(line):
    if c == 0 or c == len(line) -1:
      pass
    elif line[c-1] == line[c+1]:
      return True
  return False

for line in puzzle_in:
  line = line.split()[0]

  if pair(line) == False or between(line) == False:
    naughty += 1

print(f"Total number of nice strings using new rules is {len(puzzle_in) - naughty}")
