with open("input.txt") as file:
  puzzle_in = file.read()

floor = 0

for c, v in enumerate(puzzle_in):
  if v == "(":
    floor += 1
  if v == ")":
    floor -= 1

print(f"Santa will end up on floor {floor}.")

floor = 0

for c, v in enumerate(puzzle_in):
  if v == "(":
    floor += 1
  if v == ")":
    floor -= 1
  if floor < 0:
    basement = c
    break

print(f"Santa will first enter the basement at position {basement + 1}.")
